.PHONY: setup test clean all format check_format build publish coverage_report code_quality security_checks lint

# Set the name of the python environment directory
VENV := .venv

# Set the name of the python executable
PYTHON := python3

all: clean setup format lint code_quality security_checks test coverage_report

# Setup virtual environment and install dependencies
setup: $(VENV)/bin/activate

$(VENV)/bin/activate: make.requirements.txt
	@test -d $(VENV) || $(PYTHON) -m venv $(VENV)
	@. $(VENV)/bin/activate; pip install -Ur make.requirements.txt
	@touch $(VENV)/bin/activate

test: clean setup
	@echo "Running tests..."
	@. $(VENV)/bin/activate; coverage run -m pytest

coverage_report:
	@echo "Generating coverage report..."
	@. $(VENV)/bin/activate; coverage run -m pytest && coverage report && coverage html
	@echo "Coverage report generated in htmlcov/index.html"

format: setup
	@echo "Formatting code with black..."
	@. $(VENV)/bin/activate; black src tests

check_format: setup
	@. $(VENV)/bin/activate; black --check src tests

lint: setup
	@echo "Running Pylint..."
	@. $(VENV)/bin/activate; pylint --disable=missing-module-docstring,missing-class-docstring,missing-function-docstring,too-few-public-methods --max-line-length=110 src

code_quality: setup
	@echo "Running code quality checks..."
	@. $(VENV)/bin/activate; flake8 src tests --max-line-length=110 --exclude=__init__.py --extend-ignore=F401

security_checks: setup
	@echo "Running security checks..."
	@. $(VENV)/bin/activate; bandit -r src

check_coverage: setup
	@echo "Checking test coverage..."
	@. $(VENV)/bin/activate; coverage report --fail-under=100

build: setup
	@. $(VENV)/bin/activate; python -m build

publish: setup
	@. $(VENV)/bin/activate; python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*

clean:
	@echo "Removing virtual environment and other generated files..."
	@rm -rf $(VENV) assets .venv dist src/best_ec2.egg-info report.html htmlcov .coverage
