import pytest
from best_ec2.filters import BurstableFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"BurstablePerformanceSupported": True},
            {"burstable": True},
            True,
            "happy_path_1",
        ),
        (
            {"BurstablePerformanceSupported": False},
            {"burstable": False},
            True,
            "happy_path_2",
        ),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_burstable_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    burstable_filter = BurstableFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = burstable_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"BurstablePerformanceSupported": True},
            {"burstable": False},
            False,
            "edge_case_1",
        ),
        (
            {"BurstablePerformanceSupported": False},
            {"burstable": True},
            False,
            "edge_case_2",
        ),
        ({"BurstablePerformanceSupported": True}, {}, True, "edge_case_3"),
    ],
    ids=["edge_case_1", "edge_case_2", "edge_case_3"],
)
def test_burstable_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    burstable_filter = BurstableFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = burstable_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        ({}, {"burstable": True}, KeyError, "error_case_missing_burstable_performance"),
    ],
    ids=["error_case_missing_burstable_performance"],
)
def test_burstable_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    burstable_filter = BurstableFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        burstable_filter.apply(instance, request)
