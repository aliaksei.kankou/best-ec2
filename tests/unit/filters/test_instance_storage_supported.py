import pytest
from best_ec2.filters import InstanceStorageSupportedFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"InstanceStorageSupported": True},
            {"is_instance_storage_supported": True},
            True,
            "happy_path_1",
        ),
        (
            {"InstanceStorageSupported": False},
            {"is_instance_storage_supported": False},
            True,
            "happy_path_2",
        ),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_instance_storage_supported_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    instance_storage_filter = InstanceStorageSupportedFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = instance_storage_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"InstanceStorageSupported": True},
            {"is_instance_storage_supported": None},
            True,
            "edge_case_1",
        ),
        (
            {"InstanceStorageSupported": False},
            {"is_instance_storage_supported": None},
            True,
            "edge_case_2",
        ),
    ],
    ids=["edge_case_1", "edge_case_2"],
)
def test_instance_storage_supported_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    instance_storage_filter = InstanceStorageSupportedFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = instance_storage_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {},
            {"is_instance_storage_supported": True},
            KeyError,
            "error_case_missing_instance_storage_support",
        ),
    ],
    ids=["error_case_missing_instance_storage_support"],
)
def test_instance_storage_supported_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    instance_storage_filter = InstanceStorageSupportedFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        instance_storage_filter.apply(instance, request)
