import pytest
from best_ec2.filters import MemoryFilter
from best_ec2.constants import MEBIBYTES_IN_GIBIBYTE
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"MemoryInfo": {"SizeInMiB": MEBIBYTES_IN_GIBIBYTE * 5}},
            {"memory_gb": 4},
            True,
            "happy_path_1",
        ),
        (
            {"MemoryInfo": {"SizeInMiB": MEBIBYTES_IN_GIBIBYTE * 3}},
            {"memory_gb": 2},
            True,
            "happy_path_2",
        ),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_memory_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    memory_filter = MemoryFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = memory_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"MemoryInfo": {"SizeInMiB": MEBIBYTES_IN_GIBIBYTE}},
            {"memory_gb": 1},
            True,
            "edge_case_1",
        ),
        (
            {"MemoryInfo": {"SizeInMiB": MEBIBYTES_IN_GIBIBYTE * 3 - 1}},
            {"memory_gb": 3},
            False,
            "edge_case_2",
        ),
    ],
    ids=["edge_case_1", "edge_case_2"],
)
def test_memory_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    memory_filter = MemoryFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = memory_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_exception, test_id",
    [
        ({"MemoryInfo": {}}, {"memory_gb": 1}, KeyError, "error_case_1"),
    ],
    ids=["error_case_1"],
)
def test_memory_filter_apply_error_cases(
    instance_info, request_info, expected_exception, test_id
):
    memory_filter = MemoryFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_exception):
        memory_filter.apply(instance, request)
