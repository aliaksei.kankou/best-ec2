import pytest
from best_ec2.filters import CurrentGenerationFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"CurrentGeneration": True},
            {"is_current_generation": True},
            True,
            "happy_path_1",
        ),
        (
            {"CurrentGeneration": False},
            {"is_current_generation": True},
            False,
            "happy_path_2",
        ),
        (
            {"CurrentGeneration": False},
            {"is_current_generation": False},
            True,
            "happy_path_3",
        ),
    ],
    ids=["happy_path_1", "happy_path_2", "happy_path_3"],
)
def test_current_generation_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    current_generation_filter = CurrentGenerationFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = current_generation_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"CurrentGeneration": True},
            {"is_current_generation": False},
            False,
            "edge_case_1",
        ),
        (
            {"CurrentGeneration": False},
            {"is_current_generation": True},
            False,
            "edge_case_2",
        ),
        ({}, {"is_current_generation": None}, True, "edge_case_3"),
        ({}, {}, True, "edge_case_4"),
        ({"CurrentGeneration": True}, {}, True, "error_case_5"),
    ],
    ids=["edge_case_1", "edge_case_2", "edge_case_3", "edge_case_4", "error_case_5"],
)
def test_current_generation_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    current_generation_filter = CurrentGenerationFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = current_generation_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        ({}, {"is_current_generation": True}, KeyError, "error_case_1"),
    ],
    ids=["error_case_1"],
)
def test_current_generation_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    current_generation_filter = CurrentGenerationFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        current_generation_filter.apply(instance, request)
