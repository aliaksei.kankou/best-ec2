import pytest
from best_ec2.filters import SpotFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"InterruptionFrequency": {"min": 0.2}},
            {"usage_class": "on-demand", "max_interruption_frequency": 0.5},
            True,
            "happy_path_1",
        ),
        (
            {"InterruptionFrequency": {"min": 0.1}},
            {"usage_class": "spot", "max_interruption_frequency": 0.2},
            True,
            "happy_path_2",
        ),
        (
            {"InterruptionFrequency": {"min": 0.3}},
            {"usage_class": "spot", "max_interruption_frequency": 0.2},
            False,
            "happy_path_3",
        ),
    ],
    ids=["happy_path_1", "happy_path_2", "happy_path_3"],
)
def test_spot_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    spot_filter = SpotFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = spot_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"InterruptionFrequency": {"min": 0.2}},
            {"usage_class": "on-demand", "max_interruption_frequency": None},
            True,
            "edge_case_1",
        ),
        (
            {"InterruptionFrequency": {"min": 0.1}},
            {"usage_class": "spot", "max_interruption_frequency": None},
            True,
            "edge_case_2",
        ),
        (
            {"InterruptionFrequency": {"min": 0.3}},
            {"usage_class": "spot", "max_interruption_frequency": None},
            True,
            "edge_case_3",
        ),
        (
            {"InterruptionFrequency": {"min": 0.2}},
            {"usage_class": "spot", "max_interruption_frequency": 0.1},
            False,
            "edge_case_4",
        ),
    ],
    ids=["edge_case_1", "edge_case_2", "edge_case_3", "edge_case_4"],
)
def test_spot_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    spot_filter = SpotFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = spot_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {},
            {"usage_class": "spot", "max_interruption_frequency": 0.2},
            KeyError,
            "error_case_1",
        ),
        (
            {"InterruptionFrequency": {}},
            {"usage_class": "spot", "max_interruption_frequency": 0.3},
            KeyError,
            "error_case_2",
        ),
    ],
    ids=["error_case_1", "error_case_2"],
)
def test_spot_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    spot_filter = SpotFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        spot_filter.apply(instance, request)
