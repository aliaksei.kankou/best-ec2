import pytest
from best_ec2.filters import VcpuFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        ({"VCpuInfo": {"DefaultVCpus": 4}}, {"vcpu": 2}, True, "happy_path_1"),
        ({"VCpuInfo": {"DefaultVCpus": 1}}, {"vcpu": 1}, True, "happy_path_2"),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_vcpu_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    vcpu_filter = VcpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = vcpu_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        ({"VCpuInfo": {"DefaultVCpus": 2}}, {"vcpu": 4}, False, "edge_case_1"),
    ],
    ids=["edge_case_1"],
)
def test_vcpu_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    vcpu_filter = VcpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = vcpu_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_exception, test_id",
    [
        ({"VCpuInfo": {}}, {"vcpu": 1}, KeyError, "error_case_1"),
    ],
    ids=["error_case_1"],
)
def test_vcpu_filter_apply_error_cases(
    instance_info, request_info, expected_exception, test_id
):
    vcpu_filter = VcpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_exception):
        vcpu_filter.apply(instance, request)
