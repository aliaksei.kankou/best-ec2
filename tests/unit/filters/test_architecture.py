import pytest
from best_ec2.filters import ArchitectureFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"ProcessorInfo": {"SupportedArchitectures": ["x86_64", "arm64"]}},
            {"architecture": "x86_64"},
            True,
            "happy_path_1",
        ),
        (
            {"ProcessorInfo": {"SupportedArchitectures": ["x86_64", "arm64"]}},
            {"architecture": "arm64"},
            True,
            "happy_path_2",
        ),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_architecture_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    architecture_filter = ArchitectureFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = architecture_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"ProcessorInfo": {"SupportedArchitectures": ["x86_64", "arm64"]}},
            {"architecture": "sparc"},
            False,
            "edge_case_1",
        ),
        (
            {"ProcessorInfo": {"SupportedArchitectures": ["x86_64", "arm64"]}},
            {"architecture": None},
            True,
            "edge_case_2",
        ),
    ],
    ids=["edge_case_1", "edge_case_2"],
)
def test_architecture_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    architecture_filter = ArchitectureFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = architecture_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        ({"ProcessorInfo": {}}, {"architecture": "x86_32"}, KeyError, "error_case_1"),
        (
            {"ProcessorInfo": {"SupportedArchitectures": []}},
            {"architecture": "arm64"},
            ValueError,
            "error_case_2",
        ),
    ],
    ids=["error_case_1", "error_case_2"],
)
def test_architecture_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    architecture_filter = ArchitectureFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        architecture_filter.apply(instance, request)
