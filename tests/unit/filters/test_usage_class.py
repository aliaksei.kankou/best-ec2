import pytest
from best_ec2.filters import UsageClassFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"SupportedUsageClasses": ["on-demand", "spot"]},
            {"usage_class": "on-demand"},
            True,
            "happy_path_1",
        ),
        (
            {"SupportedUsageClasses": ["spot", "reserved"]},
            {"usage_class": "spot"},
            True,
            "happy_path_2",
        ),
    ],
    ids=["happy_path_1", "happy_path_2"],
)
def test_usage_class_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    usage_class_filter = UsageClassFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = usage_class_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"SupportedUsageClasses": ["on-demand", "spot"]},
            {"usage_class": ""},
            False,
            "edge_case_2",
        ),
    ],
    ids=["edge_case_1"],
)
def test_usage_class_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    usage_class_filter = UsageClassFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = usage_class_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_exception, test_id",
    [
        (
            {},
            {"usage_class": "on-demand"},
            KeyError,
            "error_case_missing_supported_usage_classes",
        ),
        (
            {"SupportedUsageClasses": []},
            {"usage_class": "on-demand"},
            ValueError,
            "error_case_empty_supported_usage_classes",
        ),
    ],
    ids=[
        "error_case_missing_supported_usage_classes",
        "error_case_empty_supported_usage_classes",
    ],
)
def test_usage_class_filter_apply_error_cases(
    instance_info, request_info, expected_exception, test_id
):
    usage_class_filter = UsageClassFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_exception):
        usage_class_filter.apply(instance, request)
