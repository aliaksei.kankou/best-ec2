import pytest
from best_ec2.filters import GpuFilter
from best_ec2.types import InstanceTypeInfo, InstanceTypeRequest


# Parametrized test for happy path scenarios
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {
                "GpuInfo": {
                    "TotalGpuMemoryInMiB": 8192,
                    "Gpus": [{"Count": 2}, {"Count": 1}],
                }
            },
            {"has_gpu": True, "gpu_memory": 4, "gpus": 2},
            True,
            "happy_path_1",
        ),
        (
            {"GpuInfo": {"TotalGpuMemoryInMiB": 4096, "Gpus": [{"Count": 1}]}},
            {"has_gpu": True, "gpu_memory": 2, "gpus": 1},
            True,
            "happy_path_2",
        ),
        ({}, {"has_gpu": False, "gpu_memory": 2, "gpus": 1}, True, "happy_path_3"),
    ],
    ids=["happy_path_1", "happy_path_2", "happy_path_3"],
)
def test_gpu_filter_apply_happy_path(
    instance_info, request_info, expected_result, test_id
):
    gpu_filter = GpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = gpu_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for edge cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {
                "GpuInfo": {
                    "TotalGpuMemoryInMiB": 8192,
                    "Gpus": [{"Count": 2}, {"Count": 1}],
                }
            },
            {"has_gpu": True, "gpu_memory": 8, "gpus": 3},
            True,
            "edge_case_1",
        ),
        (
            {"GpuInfo": {"TotalGpuMemoryInMiB": 4095, "Gpus": [{"Count": 1}]}},
            {"has_gpu": True, "gpu_memory": 4, "gpus": 2},
            False,
            "edge_case_2",
        ),
        ({}, {"has_gpu": None, "gpu_memory": 4, "gpus": 2}, True, "edge_case_3"),
        ({}, {"has_gpu": True, "gpu_memory": 4, "gpus": 2}, False, "edge_case_4"),
    ],
    ids=["edge_case_1", "edge_case_2", "edge_case_3", "edge_case_4"],
)
def test_gpu_filter_apply_edge_cases(
    instance_info, request_info, expected_result, test_id
):
    gpu_filter = GpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    result = gpu_filter.apply(instance, request)

    assert result == expected_result


# Parametrized test for error cases
@pytest.mark.parametrize(
    "instance_info, request_info, expected_result, test_id",
    [
        (
            {"GpuInfo": {"Gpus": None}},
            {"has_gpu": True, "gpu_memory": 4, "gpus": 2},
            KeyError,
            "error_case_1",
        ),
        (
            {"GpuInfo": {"Gpus": [{"Count": 2}, {"Count": 1}]}},
            {"has_gpu": True, "gpu_memory": 4},
            KeyError,
            "error_case_2",
        ),
    ],
    ids=["error_case_1", "error_case_2"],
)
def test_gpu_filter_apply_error_cases(
    instance_info, request_info, expected_result, test_id
):
    gpu_filter = GpuFilter()
    instance = InstanceTypeInfo(**instance_info)
    request = InstanceTypeRequest(**request_info)

    with pytest.raises(expected_result):
        gpu_filter.apply(instance, request)
